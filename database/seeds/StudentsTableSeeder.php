<?php

use Illuminate\Database\Seeder;
use App\Modules\Students\Student;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
			'first_name' => 'Славко',
			'last_name' => 'Казаков',
			'birthdate' => '1993-06-16',
			'picture_path' => '1.jpg',
			'id_school' => 2
		]);

		Student::create([
			'first_name' => 'Петре',
			'last_name' => 'Петров',
			'birthdate' => '1999-08-23',
			'picture_path' => '2.jpg',
			'id_school' => 1
		]);
    }
}
