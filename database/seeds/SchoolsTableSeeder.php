<?php

use Illuminate\Database\Seeder;
use App\Modules\Schools\School;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
    		'name' => 'Орце Николов',
    		'address' => 'Илинденска 12/1',
    		'max_students' => 413,
    		'yearly_fee' => 500.00
    	]);

    	School::create([
    		'name' => 'Блаже Конески',
    		'address' => 'Партизанска 122',
    		'max_students' => 95,
    		'yearly_fee' => 475.00
    	]);

    	School::create([
    		'name' => 'Кочо Рацин',
    		'address' => 'Благој Ѓорев 156 - Велес',
    		'max_students' => 560,
    		'yearly_fee' => 360.00
    	]);

    	School::create([
    		'name' => 'Yahya Kemal',
    		'address' => 'Партизанска',
    		'max_students' => 800,
    		'yearly_fee' => 950.00
    	]);
    }
}
