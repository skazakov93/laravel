@extends('app')

@section('content')
	<h1>School Details</h1>

	<div class="row">
		<div class="col-md-12">
			<label>Име:</label>
			{{ $school->name }}
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label>Адреса:</label>
			{{ $school->address }}
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label>Максимален број на ученици:</label>
			{{ $school->max_students }}
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label>Цена на годишно ниво:</label>
			{{ $school->yearly_fee }}$
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<a href="{{ route('createSchool') }}">new school</a>
		</div>

		<div class="col-md-6">
			<a href="{{ route('editSchoolPage', [$school->id]) }}">edit school data</a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h1>Students</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table>

				<tr>
				    <th>Име и Презиме</th>
					<th>Акција</th>
				</tr>

				@foreach($studentsInSchool as $student)
					<tr>
						<td>
							{{ $student->first_name }} {{ $student->last_name }}
						</td>
						<td>
							<a href="{{ route('editStudent', [$student->id]) }}">Повеќе</a>
						</td>
					</tr>
				@endforeach

			</table>
		</div>
	</div>
@stop