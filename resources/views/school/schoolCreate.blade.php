@extends('app')

@section('content')
	{!! Form::open(['url' => '/school/insert/']) !!}
		@include('partials.crateSchoolForm', ['buttonText' => 'Insert'])
	{!! Form::close() !!}
@stop
