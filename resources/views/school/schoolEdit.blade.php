@extends('app')

@section('content')
	{!! Form::open(['url' => '/school/update/' . $school->id]) !!}
		@include('partials.crateSchoolForm', ['buttonText' => 'Update'])
	{!! Form::close() !!}
@stop
