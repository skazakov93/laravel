@extends('app')

@section('content')
	<h1>Schools</h1>

	<table>

		<tr>
		    <th>Име</th>
		    <th>Адреса</th> 
			<th>Максимален број на ученици</th>
			<th>Акција</th>
		</tr>

		@foreach($schools as $school)
			<tr>
				<td>
					{{ $school->name }}
				</td>
				<td>
					{{ $school->address }}
				</td>
				<td>
					{{ $school->max_students }}
				</td>
				<td>
					<a href="{{ route('schoolDetails', [$school->id]) }}">Повеќе</a>
				</td>
			</tr>
		@endforeach

	</table>
@stop