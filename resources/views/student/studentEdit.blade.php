@extends('app')

@section('content')
	{!! Form::open(['url' => '/student/update/' . $student->id]) !!}
		@include('partials.crateStudentForm', ['buttonText' => 'Update'])
	{!! Form::close() !!}
@stop
