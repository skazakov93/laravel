@extends('app')

@section('content')
	{!! Form::open(['url' => '/student/insert/']) !!}
		@include('partials.crateStudentForm', ['buttonText' => 'Insert'])
	{!! Form::close() !!}
@stop
