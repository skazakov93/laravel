<div class="container">
      <div class="row">
        <div class="col-md-12">          
            
          <div class="row">            
            <div class="col-md-12">  
              
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

					       <input type="text" class="form-control" required name="name" value="{{ old('name') != '' ? old('name') : $school->name }}" placeholder="Име">
					
                   	@if ($errors->has('name'))
                   		<span class="help-block">
                          	<strong>{{ $errors->first('name') }}</strong>
                       	</span>
                   	@endif
               </div>
              
              <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
					         
                   <input type="text" class="form-control f" name="address" value="{{ old('address') != '' ? old('address') : $school->address }}" placeholder="Адреса">

                   	@if ($errors->has('address'))
                 		<span class="help-block">
                        	<strong>{{ $errors->first('address') }}</strong>
                       	</span>
                  	@endif
              </div>

              <div class="form-group{{ $errors->has('max_students') ? ' has-error' : '' }}">
                   
                   <input type="number" class="form-control f" name="max_students" value="{{ old('max_students') != '' ? old('max_students') : $school->max_students }}" placeholder="Максимален број на студенти">

                    @if ($errors->has('max_students'))
                    <span class="help-block">
                          <strong>{{ $errors->first('max_students') }}</strong>
                        </span>
                    @endif
              </div>

              <div class="form-group{{ $errors->has('yearly_fee') ? ' has-error' : '' }}">
                   
                   <input type="number" class="form-control f" name="yearly_fee" value="{{ old('yearly_fee') != '' ? old('yearly_fee') : $school->yearly_fee }}" placeholder="Годишна цена на школарината">

                    @if ($errors->has('yearly_fee'))
                    <span class="help-block">
                          <strong>{{ $errors->first('yearly_fee') }}</strong>
                        </span>
                    @endif
              </div>
                
              
              <button type="submit" class="btn submit btn-success">{{ $buttonText }}</button>
            </div>
          </div> 

        </div>
        
    </div>
</div>
