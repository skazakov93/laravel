<div class="container">
      <div class="row">
        <div class="col-md-12">          
            
          <div class="row">            
            <div class="col-md-12">  
              
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">

					       <input type="text" class="form-control" name="first_name" value="{{ old('first_name') != '' ? old('first_name') : $student->first_name }}" placeholder="Име">
					
                   	@if ($errors->has('first_name'))
                   		<span class="help-block">
                          	<strong>{{ $errors->first('first_name') }}</strong>
                       	</span>
                   	@endif
               </div>
              
              <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
					         
                   <input type="text" class="form-control f" name="last_name" value="{{ old('last_name') != '' ? old('last_name') : $student->last_name }}" placeholder="Презиме">

                   	@if ($errors->has('last_name'))
                 		<span class="help-block">
                        	<strong>{{ $errors->first('last_name') }}</strong>
                       	</span>
                  	@endif
              </div>

              <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                   
                   <input type="date" class="form-control f" name="birthdate" value="{{ old('birthdate') != '' ? old('birthdate') : $student->birthdate }}" placeholder="Датум на раѓање">

                    @if ($errors->has('birthdate'))
                    <span class="help-block">
                          <strong>{{ $errors->first('birthdate') }}</strong>
                        </span>
                    @endif
              </div>

              <div class="form-group{{ $errors->has('id_school') ? ' has-error' : '' }}">
                   
                   {!! Form::select('id_school', ['' => 'Одберете училиште'] + $schools, $student->id_school, ['class' => 'form-control', 'required' => true]) !!}

                    @if ($errors->has('id_school'))
                    <span class="help-block">
                          <strong>{{ $errors->first('id_school') }}</strong>
                        </span>
                    @endif
              </div>
                
              <div class="form-group{{ $errors->has('picture_path') ? ' has-error' : '' }}">
                 
                {{Form::file('picture_path', array('id' => 'exampleInputFile'))}}
              
                    @if ($errors->has('picture_path'))
                      <span class="help-block">
                            <strong>{{ $errors->first('picture_path') }}</strong>
                        </span>
                    @endif
                </div>
              
              <button type="submit" class="btn submit btn-success">{{ $buttonText }}</button>
            </div>
          </div> 

        </div>
        
    </div>
</div>
