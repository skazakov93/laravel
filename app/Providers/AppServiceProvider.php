<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $models = array(
            'Students',
        );

        foreach ($models as $model) {
            $this->app->bind("App\Modules\\{$model}\\{$model}RepositoryInterface", "App\Modules\\{$model}\\{$model}Repository");
        }
    }
}
