<?php

namespace App\Modules\Schools;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Students\Student;

class School extends Model
{

    public function getAllSchools() {

    	$schools = School::getAll();

    	return $schools;
    }

    public function students() {
    	return $this->hasMany(Student::class, 'id_school', 'id');
    }
}
