<?php

namespace App\Modules\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Schools\School;
use App\Modules\Students\Student;
use App\Modules\Schools\CreateSchoolRequest;

class SchoolsController extends Controller
{
    public function getAllSchools() {
    	$schools = School::all();

    	return view('school.schoolsList')->with([
    		'schools' => $schools
    	]);
    }

    public function detailsPage($id) {
    	$school = School::findOrFail($id);

    	$studentsInSchool = $school->students;

    	return view('school.schoolDetails')->with([
    		'school' => $school,
    		'studentsInSchool' => $studentsInSchool
    	]);
    }

    public function editSchoolPage($id) {
    	$school = School::findOrFail($id);

    	return view('school.schoolEdit')->with([
    		'school' => $school
    	]);
    }

    public function updateSchool(CreateSchoolRequest $request, $id) {
    	$school = School::findOrFail($id);

    	$school->name = $request->input('name');
    	$school->address = $request->input('address');
    	$school->max_students = $request->input('max_students');
    	$school->yearly_fee = $request->input('yearly_fee');

    	$school->save();

    	return redirect()->route('schoolDetails', ['id' => $school->id]);
    }

    public function createSchool() {
    	$school = new School();

    	return view('school.schoolCreate')->with([
    		'school' => $school
    	]);
    }

    public function insertSchool(CreateSchoolRequest $request) {
    	$school = new School();

    	$school->name = $request->input('name');
    	$school->address = $request->input('address');
    	$school->max_students = $request->input('max_students');
    	$school->yearly_fee = $request->input('yearly_fee');

    	$school->save();
    	
    	return redirect()->route('schoolDetails', ['id' => $school->id]);
    }
}
