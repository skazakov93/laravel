<?php

namespace App\Modules\Students;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Students\Student;
use App\Modules\Schools\School;
use App\Modules\Students\CreateStudentRequest;
use App\Modules\Students\StudentsRepositoryInterface;

class StudentsController extends Controller
{
	private $studentsInterface;

	public function __construct(StudentsRepositoryInterface $studentsInterface)
	{
		$this->studentsInterface = $studentsInterface;
	}

    public function editStudent($id) {
    	$student = Student::findOrFail($id);

    	$schools = School::pluck('name', 'id')->all();

    	return view('student.studentEdit')->with([
    		'student' => $student,
    		'schools' => $schools
    	]);
    }

    public function updateStudent(CreateStudentRequest $request, $id) {
    	$updatedStudentId = $this->studentsInterface->addStudentToSchool($request, $id);

    	return redirect()->route('editStudent', ['id' => $id])->with([
    		'updatedStudentId' => $updatedStudentId
    	]);
    }

    public function createStudent() {
    	$student = new Student();

    	$schools = School::pluck('name', 'id')->all();

    	return view('student.studentCreate')->with([
    		'student' => $student,
    		'schools' => $schools
    	]);
    }

    public function insertStudent(CreateStudentRequest $request) {
    	$updatedStudentId = $this->studentsInterface->addStudentToSchool($request, null);

    	return redirect()->route('editStudent', ['id' => $updatedStudentId])->with([
    		'updatedStudentId' => $updatedStudentId
    	]);
    }

    public function studentDetails($id) {
        $student = Student::findOrFail($id);

        return $student;
    }
}
