<?php

namespace App\Modules\Students;

interface StudentsRepositoryInterface
{
    public function addStudentToSchool(CreateStudentRequest $request, $studentId);
}
