<?php

namespace App\Modules\Students;
use App\Modules\Students\StudentsRepositoryInterface;
use App\Modules\Students\Student;
use App\Modules\Schools\School;

class StudentsRepository implements StudentsRepositoryInterface
{
    public function addStudentToSchool(CreateStudentRequest $request, $studentId) {
    	$school = School::findOrFail($request->input('id_school'));

    	$updatedStudentId = 0;
    	if (count($school->students) < $school->max_students) {
    		if(is_null($studentId)) {
    			$student = new Student();

    		}
    		else {
    			$student = Student::findOrFail($studentId);
    		}

	    	$student->first_name = $request->input('first_name');
	    	$student->last_name = $request->input('last_name');
	    	$student->birthdate = $request->input('birthdate');
	    	$student->id_school = $request->input('id_school');
	    	$student->picture_path = $request->input('picture_path');

	    	$student->save();	

	    	$updatedStudentId = $student->id;
    	}

    	return $updatedStudentId;
    }
}
