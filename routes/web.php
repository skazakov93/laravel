<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('schools', 'Schools\SchoolsController@getAllSchools');

Route::get('school/{id}', 'Schools\SchoolsController@detailsPage')->name('schoolDetails');

Route::get('school/edit/{id}', 'Schools\SchoolsController@editSchoolPage')->name('editSchoolPage');

Route::post('school/update/{id}', 'Schools\SchoolsController@updateSchool')->name('updateSchool');

Route::get('create/school', 'Schools\SchoolsController@createSchool')->name('createSchool');

Route::post('school/insert', 'Schools\SchoolsController@insertSchool')->name('insertSchool');

Route::get('student/edit/{id}', 'Students\StudentsController@editStudent')->name('editStudent');

Route::post('student/update/{id}', 'Students\StudentsController@updateStudent')->name('updateStudent');

Route::get('create/student', 'Students\StudentsController@createStudent')->name('createStudent');

Route::post('student/insert', 'Students\StudentsController@insertStudent')->name('insertStudent');